# Lab 4

We have 2 branches here in this upstream repo for this lab:
- lab3-solution-good
- lab3-solution-bad

They both change `labs/lab3*` files. Seems like 2 different people worked on
the same files. It's possible there will be merge conflicts.

Check out both branches locally so you can work with them:
```
$ git fetch upstream
$ git branch lab3-solution-bad upstream/lab3-solution-bad
$ git branch lab3-solution-good upstream/lab3-solution-good
```

Once you go from task 1 to task 2 or vice versa, make sure to reset your local branches:
```
$ git switch lab3-solution-bad
$ git reset --hard upstream/lab3-solution-bad
```


## Task 1

Switch to `lab3-solution-bad` and rebase against `lab3-solution-good`.

## Task 2

Switch to `lab3-solution-good` and rebase against `lab3-solution-bad`.

Was there a difference compared to task 1?

## Task 3

Use interactive rebase to clean up commit history of `lab3-solution-bad` (top 4 commits).
