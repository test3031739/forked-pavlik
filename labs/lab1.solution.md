# Lab 1

## Solution

### Step 1: Generate SSH Key and Upload Pubkey

SSH keys provide a secure and convenient way to authenticate with Git repositories. They eliminate the need to enter passwords each time you interact with a remote repository, making the process more efficient and secure.

Open your terminal or command prompt and run the following command:

```bash
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```

This command will generate an RSA key pair with a 4096-bit key size, and you'll be prompted to save the key in a specific location (e.g., `~/.ssh/id_rsa`) and set a passphrase (optional but recommended for added security).

Run the following command to print your public key:

```bash
cat ~/.ssh/id_rsa.pub
```

Copy the entire output, which represents your SSH public key, to the clipboard.

- Go to your git-hosting platform (gitforge)
- Find "SSH Keys" section in the settings.
- Paste your SSH public key into the "Key" field.

### Step 2: Configure Git

Configuring Git with your user name and email is essential because Git uses this information to identify you as the author of commits.

Git will not work without this: you won't be able to commit!

In your terminal or command prompt, run the following commands with your own information:

```bash
git config --global user.name "Your Name"
git config --global user.email "your_email@example.com"
```

It's useful to see the current configuration:
```bash
git config -l
```

### Step 3: Initialize a Git Repository

A Git repository is a directory that contains all the necessary files and metadata to track changes in your project. It allows you to version control your project and collaborate with others effectively.

Create a new directory for your project, navigate into it, and run the following command:

```bash
git init
```

This will initialize an empty Git repository in your project directory.

### Step 4: Clone a Remote Repository

Cloning via SSH is preferred over HTTPS because it uses your SSH key for authentication, making the process more secure and convenient.

In your terminal or command prompt, run the following command:

```bash
git clone git@gitlab.com:redhat/research/mastering-git.git
```

### Step 5: Managing Remotes

Remotes are references to other repositories, usually the ones on remote servers like GitLab, GitHub, or Bitbucket. They allow you to collaborate with others by fetching, pulling, and pushing changes.

To list the remote repositories associated with your local repository, run:

```bash
git remote -v
```

To add a new remote (e.g., origin) to your local repository, use the following command:

```bash
git remote add origin <remote_url>
```

To remove a remote (e.g., origin), use the following command:

```bash
git remote remove origin
```

### Step 6: Basic Git Operations

In your terminal or command prompt, navigate to your local repository, and run:

```bash
git status
```

This will show you the current state of your repository, including the modified, staged, and untracked files.

To stage changes for the next commit, use the following command:

```bash
git add <file1> <file2> ... <fileN>    # To stage specific files
git add .                              # To stage all changes
git add -A                             # This adds, modifies, and removes index entries to match the working tree.
git add -u                             # This removes as well as modifies index entries to match the working tree, but adds no new files.
```

Replace `<file1> <file2> ... <fileN>` with the names of the files you want to stage.

6.3. Explain how to move and rename files using `git mv` and `git rm`.

To move or rename a file, use the `git mv` command:

```bash
git mv old_filename new_filename
```

To remove a file from the repository, use the `git rm` command:

```bash
git rm new_filename
```

## Conclusion
Congratulations! You have completed the Practical Workshop Exercise. By
following these steps, you have set up your Git environment, generated SSH
keys, configured Git, initialized a repository, cloned a remote repository
using both HTTPS and SSH, managed remotes, and performed basic Git operations.

